// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// Створити метод getAge() який повертатиме скільки користувачеві років.
// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі)
// та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.

function createNewUser() {
  let firstName = prompt("input your firstname", "Joe");
  let lastName = prompt("input your lastname", "Black");
  let birthday = prompt(
    "input your birthday (text in dd.mm.yyyy format)",
    "19.12.1999"
  );
  let validBirthday = new Date(
    `${birthday.slice(6, 10)}-${birthday.slice(3, 5)}-${birthday.slice(0, 2)}`
  );

  let privateFirstName = firstName;
  let privateLastName = lastName;

  let newUser = {
    get firstName() {
      return privateFirstName;
    },
    get lastName() {
      return privateLastName;
    },

    setFirstName(newFirstName) {
      privateFirstName = newFirstName;
    },
    setLastName(newLastName) {
      privateLastName = newLastName;
    },

    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
    getAge() {
      let today = new Date();
      let age = today.getFullYear() - validBirthday.getFullYear();
      let m = today.getMonth() - validBirthday.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < validBirthday.getDate())) {
        age--;
      }
      return age;
    },
    getPassword() {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        validBirthday.getFullYear() +
        "."
      );
    },
  };

  return newUser;
}
let user = createNewUser();
console.log(user.getLogin());

user.setFirstName(prompt("input your new firstname ", "Death"));
user.setLastName(prompt("input your new lastname", "Dark"));

console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
